# Extending the miRNA repertoire in _Rattus norvegicus_

In the course of the CEFIC LRI Project ["XomeTox"](https://cefic-lri.org/projects/c5-xometox-evaluating-multi-omics-integration-for-assessing-rodent-thyroid-toxicity/), we encountered that the rat miRNA repertoire falls short in comparison to the other rodent model organism _Mus musculus_.

In XomeTox, we pursued a multi-omics approach to evaluate the benefits of such a method for detecting molecular response pathways and to infer comprehensive mechanistic insights. We were therefore relying on reliable and comprehensive data records for each individual omics layer.

To identify novel rat miRNAs, we utilized MIRfix curated whole miRNA family covariance models as described in [Yazbeck et al. (2019)]((https://doi.org/10.1093/bioinformatics/btz271).
We focused on miRNA families that contained at least one mammalian miRNA sequence.
The model building was based on miRBase version 21 ([miRBase](https://www.mirbase.org/)).
We utilized Infernal v1.1 [Nawrocki and Eddy (2013)](https://doi.org/10.1093/bioinformatics/btt509) to derive potential rat miRNA candidates using Rnor 6.0 (Ensembl Release 102, accession number GCA 000001895.4).

Candidate sequences were filter to ensure the accuracy and relevance of the identified sequences:

1. Candidates were filtered based on an e-value cutoff of 0.01 and a bit score threshold of 33
2. Duplicated candidates located on unfinished chromosomes were eliminated
3. Candidates overlapping with repeats annotated by [RepeatMasker](http://www.repeatmasker.org) were excluded
4. Candidates that were reverse complements of better candidates (smaller e-value) were also excluded

**Key facts of the extended miRNA repertoire**

- 341 miRNA families (39 **novel** families)
- 549 miRNA sequences (55 **novel** miRNAs)
- 10 corrected annotated miRNAs


## Renamed rat miRNAs

10 previously annotated miRNAs have been renamed in the course of this project.
They are listed in the table below and given in the file `Renaming_miRNAs.tsv`.


| old_miRNA_name | new_miRNA_name | miRNA_family |
| :------------ | :------------ | :----- |
| rno-mir-16	| rno-mir-16-2	| MIPF0000006 |
| rno-mir-365	| rno-mir-365-2	| MIPF0000061 |
| rno-mir-883	| rno-mir-883a	| MIPF0000389 |
| rno-mir-17-1	| rno-mir-17	| MIPF0000001 |
| rno-mir-17-2	| rno-mir-106a	| MIPF0000001 |
| rno-mir-135a	| rno-mir-135a-2	| MIPF0000028 |
| rno-mir-199a	| rno-mir-199a-2	| MIPF0000040 |
| rno-mir-26a	| rno-mir-26a-1	| MIPF0000043 |
| rno-mir-486	| rno-mir-486b	| MIPF0000220 |
| rno-mir-3074	| rno-mir-3074-1	| MIPF0001103 |


# Novel rat miRNAs

We identified and curated 55 additional miRNA sequences across 39 different miRNA families.
The new sequences included their proposed official name are listed below and in file `novel_miRNAs.tsv` in machinereadible format.

| Candidate_sequence | new_miRNA_name | miRNA_family |
| :----------------- | :------------- | :----------- |
| Candidate2	| rno-mir-16-1	| MIPF0000006 | 
| Candidate1	| rno-mir-365-1	| MIPF0000061 | 
| Candidate1	| rno-mir-302a	| MIPF0000071 | 
| Candidate2	| rno-mir-302b	| MIPF0000071 | 
| Candidate10	| rno-mir-692-1	| MIPF0000336 | 
| Candidate1	| rno-mir-692-2	| MIPF0000336 | 
| Candidate1	| rno-mir-684-1	| MIPF0000337 | 
| Candidate3	| rno-mir-684-2	| MIPF0000337 | 
| Candidate1	| rno-mir-883b	| MIPF0000389 | 
| Candidate1	| rno-mir-1905	| MIPF0000753 | 
| Candidate1	| rno-mir-703	| MIPF0000761 | 
| Candidate1	| rno-mir-1892	| MIPF0000799 | 
| Candidate1	| rno-mir-677	| MIPF0000811 | 
| Candidate1	| rno-mir-3070-1	| MIPF0001172 | 
| Candidate2	| rno-mir-3070-2	| MIPF0001172 | 
| Candidate1	| rno-mir-6516	| MIPF0001672 | 
| Candidate1	| rno-mir-7676-1	| MIPF0001816 | 
| Candidate1	| rno-mir-18b	| MIPF0000001 | 
| Candidate1	| rno-mir-744	| MIPF0000431 | 
| Candidate1	| rno-mir-1251	| MIPF0000621 | 
| Candidate1	| rno-mir-3154	| MIPF0001387 | 
| Candidate1	| rno-mir-3618	| MIPF0001710 | 
| Candidate1	| rno-mir-6967-1	| MIPF0002025 | 
| Candidate1	| rno-mir-135a-1	| MIPF0000028 | 
| Candidate1	| rno-mir-199b	| MIPF0000040 | 
| Candidate1	| rno-mir-26a-2	| MIPF0000043 | 
| Candidate1	| rno-mir-449b	| MIPF0000043 | 
| Candidate1	| rno-mir-431	| MIPF0000142 | 
| Candidate1	| rno-mir-367	| MIPF0000162 | 
| Candidate1	| rno-mir-486a	| MIPF0000220 | 
| Candidate1	| rno-mir-491	| MIPF0000319 | 
| Candidate1	| rno-mir-654	| MIPF0000409 | 
| Candidate1	| rno-mir-590	| MIPF0000418 | 
| Candidate1	| rno-mir-767	| MIPF0000552 | 
| Candidate1	| rno-mir-718	| MIPF0000721 | 
| Candidate1	| rno-mir-670	| MIPF0000734 | 
| Candidate1	| rno-mir-1898	| MIPF0000786 | 
| Candidate1	| rno-mir-1902	| MIPF0000791 | 
| Candidate1	| rno-mir-2861	| MIPF0000963 | 
| Candidate1	| rno-mir-3074-2	| MIPF0001103 | 
| Candidate1	| rno-mir-1231	| MIPF0001309 | 
| Candidate1	| rno-mir-6715	| MIPF0001708 | 
| Candidate1	| rno-mir-1893	| MIPF0001812 | 
| Candidate1	| rno-mir-1225	| MIPF0000445 | 
| Candidate1	| rno-mir-1282	| MIPF0000587 | 
| Candidate1	| rno-mir-1184	| MIPF0000604 | 
| Candidate1	| rno-mir-1248	| MIPF0000622 | 
| Candidate1	| rno-mir-1281	| MIPF0000695 | 
| Candidate1	| rno-mir-2404	| MIPF0000776 | 
| Candidate1	| rno-mir-3604-2	| MIPF0001050 | 
| Candidate2	| rno-mir-3604-1	| MIPF0001050 | 
| Candidate2	| rno-mir-1343	| MIPF0001206 | 
| Candidate1	| rno-mir-4444-1	| MIPF0001310 | 
| Candidate1	| rno-mir-4657	| MIPF0002009 | 
| Candidate1	| rno-mir-8986	| MIPF0002062 | 
| Candidate1	| rno-mir-449b	| MIPF0000133 | 

# Genomic coordinates

Novel miRNA sequences were blasted against the Rnor_6.0 GenBank assembly [GCA_000001895.4] which was also used in the current miRBase version.
Full-length hits without any mismatches have been included in the miRBase-derived rat gff3 file.
The updated gff3 file is denoted as `FINAL_EXTENDED_MIRNA_Annotation.rn6.gff3`.
To ensure compatibility with the newer mRatBN7.2 assembly we mapped the coordinates using CrossMap.
The resultsing gff3 file is denoted as `FINAL_EXTENDED_MIRNA_Annotation.rn7.gff3`.


# Alignments

Manually curated alignments of those families with renamed or new miRNA sequences are given in the respective subdirectory.
**Please note** renamed and novel miRNAs are already listed with their new names.

# European Nucleotide Archive

The 56 novel sequences that have not been listed in miRBase before have been submitted to the European Nucleotide Archive at EMBL-EBI.
They are accessible with the accession numbers OZ078105 - OZ078160.
The sequences will be permanently available from the ENA browser at http://www.ebi.ac.uk/ena/data/view/\<ACCESSION NUMBERS\> .

An overview of all sequences is given here: [http://www.ebi.ac.uk/ena/data/view/OZ078105-OZ078160](http://www.ebi.ac.uk/ena/data/view/OZ078105-OZ078160).

# Cite

If you make used of this extended miRNA catalogue, please cite the current release of this repository: [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.12626180.svg)](https://doi.org/10.5281/zenodo.12626180)

